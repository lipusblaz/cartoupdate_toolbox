import arcpy
import numpy as np
from generalFunctions import *


class PolyMerger:
    
    def __init__(self, in_shp, ex_data, tmp_dir):
        """
        ex_data - (string) url to existing layer of polygons
        in_shp - (string) url to shp file with new features
        tmp_dir - (string) directory path where temporary files will be saved
        """
        path_splited = in_shp.split("\\")
        self.in_shp = in_shp.split("\\")[-1]
        arcpy.env.workspace = "\\".join(path_splited[:-1])

        self.tmp_dir = tmp_dir
        self.ex_data = ex_data
    
    def run(self):
        """
        Runs process in which each features from in_shp in saved in temporary
        shapefile. Intersections are then calculated. If there is an intersection with
        single feature delete features from ex_data that intersect it. New polygon gets
        attributes from deleted features. If there are more intersecting features, attributes from one with
        largests area is taken. 
        
        """
        tmpShps = createTmpShps(self.in_shp, self.tmp_dir)
        tmpInters = self.tmp_dir+"\\inters.shp"

        for i in tmpShps:
            single_poly_geometry = getPolyGeometry(i)
            fids_to_del = getIntersectingFeatures(i, self.ex_data)
            attrs = getAttrsAndDelete(self.ex_data, fids_to_del)
            if attrs != None:
                SID = attrs[0]

            insertNewFeatures(single_poly_geometry, SID, self.ex_data)

if __name__ == "__main__":
    in_shp = r"D:\blaz\spatial_data\tests\test1.shp"
    ex_db = r"D:\blaz\spatial_data\del_ks0Copy.shp"
    tmp_dir = r"D:\blaz\spatial_data\tmp"

    pm = PolyMerger(in_shp, ex_db, tmp_dir)
    pm.run()




