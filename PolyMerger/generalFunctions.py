import arcpy
import arcgisscripting


def readShpLen(vhodni_shp):
    shape_len = 0
    readShpFids = []

    with arcpy.da.SearchCursor(vhodni_shp, "*") as sCur:
        for row in sCur:
            readShpFids.append( (shape_len, row[:]))
            shape_len += 1

    return readShpFids


def getPolyGeometry(singlePolyShp):

    with arcpy.da.SearchCursor(singlePolyShp, "SHAPE@") as scur:
        for row in scur:
            return row[0]

def insertNewFeatures(inGeom, attrs, ex_data):
    
    row = (inGeom, attrs)

    with arcpy.da.InsertCursor(ex_data, ("SHAPE@", "SID")) as icur:
        icur.insertRow(row)

    return row[0].extent

def createTmpShps(shp_dir, tmp_dir):

    readShpFids = readShpLen(shp_dir)
    newFiles = []
    for i, j in readShpFids:
        name = "tmpInputShp_" + str(i) + ".shp"
        try:
            arcpy.CreateFeatureclass_management(tmp_dir, name, "POLYGON")
        except arcgisscripting.ExecuteError:
            arcpy.Delete_management(tmp_dir+"\\"+name)
            arcpy.CreateFeatureclass_management(tmp_dir, name, "POLYGON")

        nameField = "FID"
        statement = '"' + nameField + '" = ' + str(i)

        with arcpy.da.InsertCursor(tmp_dir+"\\"+name, ("SHAPE@")) as icur:
            with arcpy.da.SearchCursor(shp_dir, ("SHAPE@"), statement) as scur:
                for row in scur:
                    icur.insertRow([row[0]])

        del icur
        del scur

        newFiles.append(tmp_dir+"\\"+name)

    return newFiles

def getIntersectingFeatures(in_shp, existing):
    '''
    ZA GJI: predvidevam, v existing obstaja enolicen identifikator stavb SID

    '''
    arcpy.env.addOutputsToMap = 0
    tmp_path = in_shp.split("\\")
    tmpInter = "inter.shp"

    try:
        arcpy.SpatialJoin_analysis(in_shp, existing, tmpInter, "JOIN_ONE_TO_MANY")
    except arcgisscripting.ExecuteError:
        arcpy.Delete_management(tmpInter)
        arcpy.SpatialJoin_analysis(in_shp, existing, tmpInter, "JOIN_ONE_TO_MANY")

    inter_sid = []

    fields = ("Join_Count", "SID") #join_count == st. presekov, SID = enolicni identifikator stavb(po katastru)

    with arcpy.da.SearchCursor(tmpInter, fields) as sCursor:
        for row in sCursor:
            if row[0] != 0:
                inter_sid.append(int(row[1]))

    arcpy.Delete_management(tmpInter)

    return inter_sid

def getAttrsAndDelete(ex_data, sids):

    attrs = None

    if len(sids) == 1:
        nameField = "SID"
        statement = '"' + nameField + '" = ' + str(sids[0])

        with arcpy.da.SearchCursor(ex_data, ("SHAPE@"), statement) as scur:
            for row in scur:
                attrs = (sids[0], row[0])

        with arcpy.da.UpdateCursor(ex_data, "SID", statement) as ucur:
            for row in ucur:
                ucur.deleteRow()


    elif len(sids) > 1:
        max_a = 0
        for i in sids:
            nameField = "SID"
            statement = '"' + nameField + '" = ' + str(i)

            with arcpy.da.SearchCursor(ex_data, ("SHAPE@", "SHAPE@AREA"), statement) as scur:
                for row in scur:
                    if row[-1] > max_a:
                        max_a = row[-1]
                        attrs = (i, row[0])

            with arcpy.da.UpdateCursor(ex_data, "*", statement) as ucur:
                for row in ucur:
                    ucur.deleteRow()

    else:
        arcpy.AddMessage("Ni prekrivanja, ne bom prevzel atributov in ne bom brisal starih polygonov.")

    return attrs