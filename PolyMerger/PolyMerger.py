import arcpy
from generalFunctions import *

class PolyMerger:

    def __init__(self, in_shp, ex_data, tmp_dir):
        """
        ex_data - (string) url to existing layer of polygons
        in_shp - (string) url to shp file with new features
        tmp_dir - (string) directory path where temporary files will be saved
        """

        path_splited = in_shp.split("\\")
        self.in_shp = in_shp.split("\\")[-1]
        arcpy.env.workspace = "\\".join(path_splited[:-1])

        self.tmp_dir = tmp_dir
        self.ex_data = ex_data
    
    def run(self):
        """
        Runs process in which each features from in_shp in saved in temporary
        shapefile. Intersections are then calculated. If there is an intersection with
        single feature delete features from ex_data that intersect it. New polygon gets
        attributes from deleted features. If there are more intersecting features, attributes from one with
        largests area is taken. 
        
        returns extents of each feature for later tile package update

        """

        arcpy.AddMessage("Creating temporary shape files")
        tmpShps = createTmpShps(self.in_shp, self.tmp_dir)

        extents = []
        arcpy.AddMessage("Reading temporary shapes.")
        for i in tmpShps:
            arcpy.AddMessage("Merging {} shape file.".format(i))
            single_poly_geometry = getPolyGeometry(i)
            fids_to_del = getIntersectingFeatures(i, self.ex_data)
            arcpy.AddMessage("Intersecting features are: {}".format(fids_to_del) )
            attrs = getAttrsAndDelete(self.ex_data, fids_to_del)
           
            if attrs != None:
                SID = attrs[0]
                insertNewFeatures(single_poly_geometry, SID, self.ex_data)
                arcpy.AddMessage("Deleted features: {}, prevzemam podatke SID-a: {}\n".format(fids_to_del, attrs[0] ))
            else:
                arcpy.AddMessage("No features deleted. Verjetno gre za novo stavbo\n")

            extent_i = insertNewFeatures(single_poly_geometry, 0, self.ex_data)
            extents.append(extent_i)
        
        #returns !extent objects! of all added polygons
        return extents

if __name__ == "__main__":
    
    #TESTS
    in_shp = r"D:\blaz\spatial_data\tests\test1.shp"
    ex_db = r"D:\blaz\spatial_data\del_ks0Copy.shp"
    tmp_dir = r"D:\blaz\spatial_data\tmp"

    pm = PolyMerger(in_shp, ex_db, tmp_dir)
    pm.run()




