# -*- coding: utf-8 -*-

#import arcpy
import sys

sys.path.append("LineMerger")
sys.path.append("PolyMerger")

#trije moduli programa- 
import PolyMerger #polygon (najmanjsi)
import LineMerger #line (najvecji)
import TileManage #posodabljanje

class updater:
    def __init__(self, shp_file_path, temp_path, ks_path, ceste_path, tile_cache_path, tile_cache_name, tile_scheme_path):
        '''
        preberi vhodne podatke in shrani v objektne spremenljivke, nastavi
        se workspace na direktorij shp datoteke
        '''
        self.input = shp_file_path

        self.tmp = temp_path
        self.ks = ks_path
        self.ceste = ceste_path

        self.tile_path = tile_cache_path
        self.tile_name = tile_cache_name
        self.scheme = tile_scheme_path

        ws = shp_file_path.split("\\")
        arcpy.env.workspace = "\\".join(ws[:-1])

    def runMerge(self):
        shp_info = arcpy.Describe(self.input)
        extents = None
        #vrnjene potrebujem extente za vsak feature.. glede na to se potem
        #generirajo tile-i na tistem obmocju

        #Izvede se ustrezna posodobitev glede na tip geometrije
        if shp_info.shapeType == "Polygon":
            arcpy.AddMessage("Novi podatki so tipa Polygon... poganjam PolyMerger")
            pm = PolyMerger.PolyMerger(self.input, self.ks, self.tmp)
            extents = pm.run()

        elif shp_info.shapeType == "Polyline":
            #ni tako hitro zaradi zadnje vrstice
            #trim/extent se izvaja na celotni bazi ne na izbranih objektih
            arcpy.AddMessage("Novi podatki so tipa Polyline... poganjam LineMerger")
            lm = LineMerger.LineMerger(self.input, self.ceste, self.tmp)
            extents = lm.run()
            lm.trim_extend()

        else:
            arcpy.AddMessage("Shape type vhodne datoteke ni pravi. Možnosti: Polygon, Polyline. Vhodna: {}".format(shp_info.shapeType))

        return extents

    #TODO: preverjanje vhodnih podatkov pri linijah/cestah. Torej izvaja se le v primeru kategorije avtoceste (ipd...). 

    def updateTiles(self, extents):
        tm = TileManage.TileManage(self.tile_path, self.tile_name, self.scheme, self.tmp)
        for ex in extents:
            tmp_ex = tm.createTmpAoi(ex)
            tm.updateTiles(tmp_ex)
        #arcpy.AddMessage(tmp_ex)
        arcpy.AddMessage("Updated all the tiles")

if __name__ == "__main__":
    #definiranje vhodnih podatkov
    #shp_file_path: pot do shp datoteke, ki se bo posodobila v bazo kartografije
    #ks_path: pot do gdb baze katastra stavb, v primeru shp-ja polygonov se uporabi
    #ceste_path: pot do gdb baze linij(ceste, zeleznice, v primeru shp-ja linij se uporabi
    #temp_path: pot do mape, kjer se shranjujejo zacasne datoteke

    #tile_cache_path: connection string do baze za tile-e
    #tile_scheme_path: configuracijska datoteka za tile-e
    #


    shp_file_path = arcpy.GetParameterAsText(0)

    ks_path = arcpy.GetParameterAsText(1)
    ceste_path = arcpy.GetParameterAsText(2)
    temp_path = arcpy.GetParameterAsText(3)

    tile_cache_path = "agstile.luz.local:6080/arcgis"
    tile_scheme_path = r".\conf.xml"
    tile_cache_name = "#"

    updt = updater(shp_file_path, temp_path, ks_path, ceste_path, tile_cache_path, tile_cache_name, tile_scheme_path)
    extents = updt.runMerge()
    updt.updateTiles