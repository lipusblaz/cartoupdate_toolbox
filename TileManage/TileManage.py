import arcpy

class TileManage:
    def __init__(self, in_cache_path, cache_name, tile_scheme_path, tmp_dir):
        self.cache_dir = in_cache_path
        self.name = cache_name
        self.scheme = tile_scheme_path
        self.tmp = tmp_dir

    def createTmpAoi(self, ext):

        ##hardcoded name... deletes after updating tiles
        name = "aoi.tmp"
        aoi_full = self.tmp+"\\"+name
        arcpy.CreateFeatureclass_management(name, self.tmp, "POLYGON")

        tmpPolygon = []
        tmpPolygon.append(arcpy.Point(ext.XMin-10, ext.YMin-10))
        tmpPolygon.append(arcpy.Point(ext.XMin-10, ext.YMax+10))
        tmpPolygon.append(arcpy.Point(ext.XMax+10, ext.YMax+10))
        tmpPolygon.append(arcpy.Point(ext.XMax+10, ext.YMin-10))
        tmpPolygon.append(arcpy.Point(ext.XMin-10, ext.YMin-10))
        arr = arcpy.Array(tmpPolygon)
        poly = arcpy.Polygon(arr)

        with arcpy.da.InsertCursor(aoi_full, ("SHAPE@")) as i_cur:
            id_control = i_cur.insertRow([poly])

        return aoi_full


    def updateTiles(self, extent):
        
        aoi = self.createTmpAoi(extent)

        in_cache_location = self.cache_dir
        manage_mode = "RECREATE_ALL_TILES"
        in_cache_name = self.name
        in_datasource = "#"
        tiling_scheme = "IMPORT_SCHEME"
        import_tiling_scheme = self.scheme
        scales = "#"
        area_of_interest = aoi #feature class with area of interest. It should be polygon, returns name
        max_cell_size = "#"
        min_cached_scale = "#"
        max_cached_scale = "#"

        
        arcpy.ManageTileCache_management(in_cache_location, manage_mode, in_cache_name, in_datasource,
            tiling_scheme, import_tiling_scheme, scales, area_of_interest, max_cell_size, min_cached_scale, max_cached_scale)


        arcpy.Delete_management(area_of_interest)

if __name__ == "__main__":
    arcpy.AddMessage("delam")