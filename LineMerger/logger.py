import os

class logger():
    def __init__(self, name):
        self.filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), name+".log")
        file = open(self.filename, "w")
        file.close()
        
    def log(self, msg):
        if type(msg) == str:
            tmp_file = open(self.filename, "a")
            tmp_file.write(msg +"\n")
            tmp_file.close()
        else:
            tmp_file = open(self.filename, "a")
            tmp_file.write("{}\n".format(msg))
            tmp_file.close()
        