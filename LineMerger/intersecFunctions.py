import arcpy
from generalFunctions import euclideanDist, dict2Polylines2
from overlapFunctions import length_polyline

def findIntersection(infc, dataset, tmp_dir):
    """
    Uporabi funkcijo vgrajeno v arcgis za iskanje presecisc med dvema linijskima
    slojema. Vhodni podatki morajo biti dejanski objekt "arcpy layer" 

    Funkcija se pozene na zacetku postopka preden se vse operacije izvajajo na 
    geometriji shranjeni v python Dict ali List

    Returns: (list) list tock podanih v listu [ [x1, y1], [x2, y2], [], ... ]
    """

    arcpy.env.addOutputsToMap = 0
    arcpy.Intersect_analysis([infc, dataset], tmp_dir + "\\preseki.shp", "ALL" , 0.0001, "POINT")

    intersections = []
    with arcpy.da.SearchCursor(tmp_dir + "\\preseki.shp", ("OID@","SHAPE@")) as sCursor:
        for row in sCursor:
            for ptn in row[1]:
                intersections.append([ptn.X, ptn.Y])
            
    arcpy.Delete_management(tmp_dir + "\\preseki.shp")
    
    return intersections

def getIntersectIndex(intersections, dataToUpdate, endpnts, odstopanje):

    keys = list(dataToUpdate.keys())
    linkData = {}
    out_data = {}
    for key in keys:
        linkData[key] = []

    for i in intersections:
        xi = i[0]
        yi = i[1]

        minD = min([euclideanDist(end, [xi,yi]) for end in endpnts])

        if minD > odstopanje: #razdalja med endpointom in intersectom vecja od odstopanje
            for part in keys:
                p = dataToUpdate[part]
                for j in range(len(p)):

                    if j == len(p)-1:
                        break

                    else:
                        distWhole = euclideanDist(p[j], p[j+1])
                        dist1 = euclideanDist(p[j], [xi, yi])
                        dist2 = euclideanDist([xi, yi], p[j+1])

                        if abs((dist1+dist2)-distWhole) < 0.1:
                            linkData[part].append([j, [xi,yi]])

    for link in linkData:
        out_data[link] = sorted(linkData[link][:], key = lambda x: x[0])

    return out_data

def update4intersect(intersections, dataToUpdate, endpnts, odstopanje):

    keys = list(dataToUpdate.keys())
    linkData = {}
    out_data = dataToUpdate.copy()


    for i in intersections:
        xi = i[0]
        yi = i[1]

        minD = min([euclideanDist(end, [xi,yi]) for end in endpnts])

        if minD > odstopanje: #razdalja med endpointom in intersectom vecja od odstopanje
            for part in keys:
                p = out_data[part][:]
                for j in range(len(p)):

                    if j == len(p)-1:
                        break

                    else:
                        distWhole = euclideanDist(p[j], p[j+1])
                        dist1 = euclideanDist(p[j], [xi, yi])
                        dist2 = euclideanDist([xi, yi], p[j+1])
                        

                        if abs(distWhole)< 0.01:
                            continue
                        elif dist1 < 0.01:
                            continue
                        elif dist2 < 0.01:
                            continue

                        elif abs((dist1+dist2)-distWhole) < 0.01:
                            p.insert(j+1, [xi,yi])
                            break

                out_data[part] = p    

    return out_data


def splitAtInter(linkData, dataToUpdate):
    """
    struktura vhodnih podatkov:

    link_data = { oid1: [[index1, [xi, yi]], ...], oid2: [[index2, [xi, yi]], ...], ...}

    dataToUpdate = { (oid, uid):[ [xx, yy], [xx, yy], ... ], (oid, uid):[...], ... }

    """ 
    ex_keys = list(dataToUpdate.keys())
    updated = {}

    del_links = []
    #za vsako exdata part, ki vsebuje presecisce
    for link in linkData:

        #ali je v exdata part id v celotnem dictu za posodobit
        if link in ex_keys and len(linkData[link]) > 0:
            poly = dataToUpdate[link][:]
            key = link[0]
            uid = 60

            if len(linkData[link]) == 1:
                    intr = linkData[link][0]
                    updated[(key, uid+1)] = poly[:intr[0]]+ [intr[1]]
                    updated[(key, uid+2)] = [intr[1]] + poly[intr[0]+1:]
                    del_links.append(link)
                    continue
            else:
                prev_ind = 0
                acc = 1
                max_ind = max([linkData[link][i][0] for i in range(len(linkData[link]))])

                for intr in linkData[link]:
                    if intr[0] == max_ind:

                        updated[(key, uid+acc+1)] = poly[ prev_ind : intr[0]+acc ] + [ intr[1] ]
                        updated[(key, uid+acc+2)] = [ intr[1] ] + poly[ intr[0]+acc : ]

                    else:
                        ind = intr[0]+acc

                        poly.insert(ind, intr[1])

                        updated[(key, uid+acc)] = poly [ prev_ind : ind ]

                        acc += 1
                        prev_ind = ind
                        del_links.append(link)


    del_links = set(del_links)

    for i in del_links:
        del dataToUpdate[i]

    dataToUpdate.update(updated)

    return dataToUpdate




