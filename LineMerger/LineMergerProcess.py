import arcpy
from datetime import datetime, date
import time
from logger import logger
from generalFunctions import *
from overlapFunctions import *
from endpointFunctions import *
from intersecFunctions import *

def mergingProcess(infc, dataset, new_attrs, tmp_dir, neigh, tolerance, dangle_len):
    """
    INPUT:
    infc = (string) path to shp file which is to be merged in existing dataset
    dataset = (string) path/name(if already inported) of existing dataset into which "infc" is merging
    new_attrs = ATR 4 and DAT_EL from original input shp file and not tmp shp
    neigh = "neighbourhood"(int or float) numeric value for selecting features close to "infc"
            for faster processing times and smaller working dataset (higher value gives more data)
    tolerance = (int or float) max distance to connect endpoints of "infc" to "dataset"
    dangle_len = ??? (int or float) max distance for which lines are extended to other lines
                    and max length of features to be classified as dangles and deleted

    other used parameters:
    - function getOverlapFeatures: - parameter dist -> dovoljeno odstopanje vecja kot je cifra vecja toleranca prekrivanja
                                    - parameter perc -> minimalno procent verteksov v blizini dist, da se feature smatra kot prekrivanje

    - parameter neigh: uporabljen v select_loc funkciji, predstavlja dolzino v kateri se izbirajo feature-i, 
                        manjsa vrednost lahko pomaga hitrosti programa, vendar obstaja moznost izgube nekaterih podatkov

    - function getIntersecIndex: parameter odstopanje -> odstopanje med intersection-om in robno tocko, uporabno za preklic
                                brisanje intersection-a v primermu premajhnega odstopanja


    """
    ############ EDGE FIXING 1 ###############
    log = logger("main")
    arcpy.AddMessage("Starting merging proces... \n")
    t00 = time.time()

    exdataVertices = select_loc(dataset, infc, neigh) #se konstantno spreminja

    arcpy.AddMessage("Found {} close line features in existing dataset:".format(len(exdataVertices)))

    for feat in exdataVertices:
        arcpy.AddMessage("OBJECT ID: {}, LENGTH: {}".format(feat, length_polyline(exdataVertices[feat])))
    arcpy.AddMessage("\n")

    infcVertices = read_in_geom(infc) #ostane enak za celoten postopek

    arcpy.AddMessage("Read input *.shp file geometry. Input feature is {} m long.".format(length_polyline(infcVertices)))

    intersecsCoord = findIntersection(infc, dataset, tmp_dir) #ostane enak za celoten postopek
    edgeInfcVertices = get_endptns(infcVertices) #ostane enak za celoten postopek

    exdataVertices = insertVerEnd(edgeInfcVertices, exdataVertices, 5)
    arcpy.AddMessage(exdataVertices.keys())
    arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

    #ne izpiljena funkcija
    matchPnts = closestPntMatch(edgeInfcVertices, exdataVertices, tolerance) #parameter tolerance

    exdataVertices = snapCloseGeom(matchPnts, exdataVertices)
    arcpy.AddMessage(exdataVertices.keys())
    arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

    exdataVertices = splitAtEdges(exdataVertices, matchPnts)
    arcpy.AddMessage(exdataVertices.keys())
    arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

    exdataVertices = splitPartialOverlap(exdataVertices, infcVertices, 5)
    arcpy.AddMessage(exdataVertices.keys())
    arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])


    exdataVertices = snapEnds(infcVertices, exdataVertices, 5)
    arcpy.AddMessage(exdataVertices.keys())
    arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

    ############ OVERLAP FIXING #################
    #overlay_features = getOverlap(infc, exdataVertices, tmp_dir, 75, tolerance)

    overlay_features = getOverlapFeatures(infcVertices, exdataVertices, 5, 7, 75)

    if len(overlay_features) != 0: 
        arcpy.AddMessage("Overlaping features before/without split:")
        for i in overlay_features:
            arcpy.AddMessage("temp. ID: {}, part length: {}, overlap %: {}".format(i, overlay_features[i][2], round(overlay_features[i][1],2)))
        arcpy.AddMessage("\n")
        
        exdataVertices, attr_ID = delOverlap(dataset, exdataVertices, overlay_features)
        isOverlap = True
    else:

        arcpy.AddMessage("No overlapping features at current state")
        arcpy.AddMessage("Splitting at intersections")

        #link_dat = getIntersectIndex(intersecsCoord, exdataVertices, edgeInfcVertices, 1.0) #PARAMETER!!!
        arcpy.AddMessage("Number of found intersections: {}".format(len(intersecsCoord)))

        linkIntersect = getIntersectIndex(intersecsCoord, exdataVertices, edgeInfcVertices, 1.0)
        exdataVertices = splitAtInter(linkIntersect, exdataVertices)
        #exdataVertices = update4intersect (intersecsCoord, exdataVertices, edgeInfcVertices, 1.0)

        #overlay_features = getOverlap(infc, exdataVertices, tmp_dir, 75, tolerance)
        overlay_features = getOverlapFeatures(infcVertices, exdataVertices, 5, 5, 75)

        if len(overlay_features) == 0:
            arcpy.AddMessage("WARNING: No features were found as overlap. Input feature won't have attributes.")
            isOverlap = False
        else:
            arcpy.AddMessage("Overlaping features after the splitsplited:")
            isOverlap = True
            for i in overlay_features:
                arcpy.AddMessage("temp. ID: {}, part length: {}, overlap %: {}".format(i, overlay_features[i][2], round(overlay_features[i][1],2)))
            arcpy.AddMessage("\n")

            exdataVertices, attr_ID = delOverlap(dataset, exdataVertices, overlay_features)

    ########## INTERSECTION FIXING ##########

    #Brisanje morebitnih polylinij z dolzino 0m - torej samo tock
    keys = [i for i in exdataVertices]

    for part in keys:
        lenn = length_polyline(exdataVertices[part])
        if lenn == 0:
            del exdataVertices[part]

    if len(intersecsCoord)>0:
        keys = [i[1] for i in exdataVertices]

        if all(key <= 60 for key in keys):
            arcpy.AddMessage("Found : {} intersections or connections!".format(len(intersecsCoord)))

            #intersecIndexDataset = getIntersectIndex(intersecsCoord, exdataVertices, edgeInfcVertices, 5) #PARAMETER(dovoljeno odstopanje) 0.2 m
            #exdataVertices= updateVerticesWithIntersection(intersecIndexDataset, exdataVertices)
            
            exdataVertices = update4intersect(intersecsCoord, exdataVertices, edgeInfcVertices, 5)
            arcpy.AddMessage("if:inter")
            arcpy.AddMessage(exdataVertices.keys())
            arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

            updateAll(dataset, exdataVertices)

        else:
            arcpy.AddMessage("else:inter")
            arcpy.AddMessage(exdataVertices.keys())
            arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])
            updateAll(dataset, exdataVertices)
    else:
        arcpy.AddMessage("No intersections found!")
        arcpy.AddMessage("\n")
        arcpy.AddMessage(exdataVertices.keys())
        arcpy.AddMessage([length_polyline(exdataVertices[i]) for i in exdataVertices])

        updateAll(dataset, exdataVertices)

    ########## INSERTING AND ADDING DELETED ATTRIBUTES ##############


    if len(overlay_features) > 0:
        insert_update_attr (dataset, infcVertices, overlay_features, attr_ID, new_attrs, True)

    else:
        noneAttrs = [None, None, None, None, None, None, None]
        insert_update_attr (dataset, infcVertices, overlay_features, noneAttrs, new_attrs, False)

    t11 = time.time()

    arcpy.AddMessage("It took {} seconds to merge this feature.".format(t11-t00))
    
    isOverlap = True
    return isOverlap

############################################################

def writeControl(row, controlClassName):
        """
        Saves extents of each importing feature to control class as polygon feature
        """
        ext = row[0].extent 
        id_control = 0
        tmpPolygon = []
        tmpPolygon.append(arcpy.Point(ext.XMin-20, ext.YMin-20))
        tmpPolygon.append(arcpy.Point(ext.XMin-20, ext.YMax+20))
        tmpPolygon.append(arcpy.Point(ext.XMax+20, ext.YMax+20))
        tmpPolygon.append(arcpy.Point(ext.XMax+20, ext.YMin-20))
        tmpPolygon.append(arcpy.Point(ext.XMin-20, ext.YMin-20))
        arr = arcpy.Array(tmpPolygon)
        poly = arcpy.Polygon(arr)

        with arcpy.da.InsertCursor(controlClassName, ("SHAPE@")) as i_cur:
            id_control = i_cur.insertRow([poly])

        with arcpy.da.UpdateCursor(controlClassName, ("OID@", "DAT_POSOD")) as uCur:
            for row in uCur:
                if row[0] == id_control:
                    row[1] = date.today()
                    uCur.updateRow(row)


def makeDatetimeObj(datestr):
    """
    Parses time in string object and saves it in datetime object
    for importing into table with date field
    """
    year = datestr[0:4]
    month = datestr[4:6]
    day = datestr[6:]

    date = datetime.strptime(year+" "+month+" "+day, "%Y %m %d")
    return date


##################################################################

def processEachFeature(path, input_shp,dataset, readShpFids, cClassPath, counter):
    """
    Creates new feature layer for control at "path". For each feature in input_shp
    creates tmp layer which is then passed in merging process function and is 
    merged in "dataset".
    """
    c_start = counter[0]
    c_stop = counter[1]
    extents = []

    for i in readShpFids[c_start:c_stop]: #celoten postopek- izdela shp in na njim klice merging process
        
        ATR4 = ""
        DAT_EL = ""
        name = "tmpInputShp_"+str(i)
        name = name+".shp"
        arcpy.CreateFeatureclass_management(path, name, "POLYLINE")
        
        nameField = "FID"
        statement = '"' + nameField + '" = ' + str(i)

        with arcpy.da.InsertCursor(path+"\\"+name, ("SHAPE@")) as i_cur:
            with arcpy.da.SearchCursor(input_shp, ("SHAPE@", "DAT_EL", "ATR4", "ATR1", "ATR2"), statement) as s_cur:
                for srow in s_cur:
                    writeControl(srow, cClassPath)
                    i_cur.insertRow([srow[0]])
                    DAT_EL = makeDatetimeObj(srow[1])
                    
                    try:
                        ATR4 = float(srow[2])
                    except ValueError:
                        arcpy.AddMessage("Ni mogoce prebrati ATR4 kot stevilo. Privzeta vrednost bo 0.0")
                        ATR4 = 0.0
                    
                    ATR1 = int(srow[3])
                    ATR2 = str(srow[4])

        #arcpy.AddMessageing input information and running merging process
        arcpy.AddMessage("\n################ NEW Feature ####################")
        arcpy.AddMessage("ATR4 oz. ODSEK: {}, Datum elaborata: {}".format(ATR4, str(DAT_EL).split()[0]))
        arcpy.AddMessage(path+"\\"+name)
        arcpy.AddMessage("###################################################")
        
        DAT_POSOD = datetime.now()
        arcpy.AddMessage(DAT_POSOD)

        #pogon glavne funkcije, ki vsebuje 3 dodatne parametre

        extents.append(arcpy.Describe(path+"\\"+name).extent)

        isOverlap = mergingProcess(path+"\\"+name, dataset,[ATR4, ATR1, DAT_EL, DAT_POSOD, ATR2], path, 7, 5, 5) ####### MAIN

        with arcpy.da.UpdateCursor(cClassPath, ["Id", "PREK"]) as upd_cur:
            for row in upd_cur:
                if row[0] == i:
                    if isOverlap:
                        row[1] = 1
                    else:
                        row[1] = 0
                    upd_cur.updateRow(row)

        #arcpy.AddMessage("New ObjectID for inserted feature = {}".format(link))
    arcpy.Delete_management(path+"\\"+name)
    
    return extents
