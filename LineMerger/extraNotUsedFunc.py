import arcpy

def getOverlap(infc, exdata, tmp_dir, perc,dist = 5):
    arcpy.env.overwriteOutput = True
    out_buff = tmp_dir+"buff_over.shp"

    arcpy.Buffer_analysis(infc, out_buff, dist)
    arcpy.MakeFeatureLayer_management (out_buff, "buff")
    overlay_feat = {}

    for part in exdata:

        part_c = exdata[part][:]

        #dodajanje verteksov za boljsi priblizek
        if len(part_c) < 10 and length_polyline(part_c) > 10:
            acc = 1
            for point in range(len(exdata[part])-1):
                p11 = exdata[part][point]
                p22 = exdata[part][point+1]

                dist_eu = euclideanDist(p11, p22)
                m, c = line_eq(p11, p22)

                if dist_eu > 11.0:

                    st_dodatnih_v = int(dist_eu/5)-1
                    rang = abs((p22[0]-p11[0])/st_dodatnih_v)

                    for dodatni in range(st_dodatnih_v):
                        x_add = p11[0]+(dodatni*rang)
                        y_add = m*x_add + c
                        part_c.insert(point+acc, [x_add, y_add])
                        acc += 1

                else:
                    x_mid = (p11[0]+p22[0])/2
                    y_mid = m*x_mid + c

                    part_c.insert(point+acc, [x_mid, y_mid])
                    acc += 1

        name0 = tmp_dir+"\\"+str(part[0])+"_"+str(part[1])+".shp"
        name = str(part[0])+"_"+str(part[1])+".shp"

        arcpy.CreateFeatureclass_management(tmp_dir, name, "POINT")

        points = [arcpy.Point(ver[0], ver[1]) for ver in part_c]

        with arcpy.da.InsertCursor(name0, ("SHAPE@")) as icur:
            for ptn in points:
                icur.insertRow([ptn])

        arcpy.MakeFeatureLayer_management(name0, name)
        arcpy.SelectLayerByLocation_management(name, "WITHIN", "buff")

        count = int(arcpy.GetCount_management(name).getOutput(0))

        arcpy.Delete_management(name0)

        percPart = float(count) / len(part_c) *100
        if percPart >= perc:
            overlay_feat[part] = [count, percPart, length_polyline(exdata[part])]

    return overlay_feat


def intersecUpdateVertices(linkData, dataToUpdate):
    """
    struktura vhodnih podatkov:

    link_data = { oid1: [index1, [xi, yi]], oid2: [index2, [xi, yi]] ...}

    dataToUpdate = { (oid, uid):[ [xx, yy], [xx, yy], ... ], (oid, uid):[...], ... }

    """ 
    uID = 60
    data_keys = list(dataToUpdate.keys())

    link_dataIDs = [j[1] for j in linkData]

    updated = {}

    for seg in data_keys:
        pres = [ [j[0], j[2]+1] for j in linkData if j[1] == seg ]

        if len(pres) == 0:
            continue

        elif len(pres) == 1:

            updated[ (seg[0], uID) ] = dataToUpdate[seg][:pres[0][1]]
            updated[ (seg[0], uID) ] = updated[(seg[0], uID) ]+[pres[0][0]]
            
            #preveri ali ostane samo en verteks
            if len(dataToUpdate[seg][pres[0][1]:]) == 1:
                #in ce ostane preveri razdaljo

                if euclideanDist(pres[0][0], dataToUpdate[seg][pres[0][1]]) > 7: #PARAMETER 7 !!!
                    continue
                else:
                    updated[ (seg[0],uID+1) ]= dataToUpdate[seg][pres[0][1]:]
                    updated[ (seg[0],uID+1) ] = [pres[0][0]] + updated[ (seg[0],uID+1) ]
                    uID += 2

            else:
                updated[ (seg[0],uID+1) ]= dataToUpdate[seg][pres[0][1]:]
                updated[ (seg[0],uID+1) ] = [pres[0][0]] + updated[ (seg[0],uID+1) ]
                uID += 2

        else:
            p0 = 0
            prev_inter = 0
            for i in range(len(pres)+1):
                
                if i == 0:
                    updated[(seg[0], uID)] = dataToUpdate[seg][p0:pres[i][1]]

                    updated[(seg[0], uID)] = updated[(seg[0], uID)] + [pres[i][0]]

                    prev_inter = pres[i][0]
                    p0 = pres[i][1]
                    uID +=1

                elif i == len(pres):
                    if euclideanDist(prev_inter, dataToUpdate[seg][p0:][0]) > 7: #PARAMETER 7 !!
                        continue
                    else:
                        updated[(seg[0], uID)] = [prev_inter] + dataToUpdate[seg][p0:]
                        uID +=1
                else:
                    
                    updated[(seg[0], uID)] = [prev_inter] + dataToUpdate[seg][p0:pres[i][1]]
                    updated[(seg[0], uID)] = updated[(seg[0], uID)] + [pres[i][0]]

                    prev_inter = pres[i][0]
                    p0 = pres[i][1]
                    uID +=1

    #primerjava vhodnih in novih in menjava
    for kk in [j[1] for j in linkData]:
        if kk in dataToUpdate:
            del dataToUpdate[kk]

    dataToUpdate.update(updated)
    
    return dataToUpdate

def updateVerticesWithIntersection(linkData, dataToUpdate):
    """
    Pomemben parameter!!! dozina pri length_polyline
    """
    ex_keys = list(dataToUpdate.keys())

    #za vsako exdata part, ki vsebuje presecisce
    for link in linkData:

        #ali je v exdata part id v celotnem dictu za posodobit
        if link in ex_keys and len(linkData[link]) > 0:
            prev_ind = 0
            acc = 1
            poly = dataToUpdate.copy()[link][:]

            #vsi linki presecisc sortirani po indeksih
            alles = sorted([linkData[link][i][0] for i in range(len(linkData[link]))])

            #za vsako presecisce 
            for intr in linkData[link]:

                if len(alles) == 1: 
                    ind = intr[0]+acc
                    poly.insert(ind, intr[1])

                    if length_polyline(poly[ind:]) < 15 and len(poly[ind+1:]) < 3:
                        poly = poly[:ind+1]


                    elif length_polyline(poly[:ind+1]) < 15 and len(poly[:ind+1]) < 3:
                        poly = poly[ind:+1]

                elif intr[0] == alles[0]:
                    ind = intr[0]+acc
                    poly.insert(ind, intr[1])

                    if length_polyline(poly[:ind]) < 15 and len(poly[:ind]) < 3:
                        poly = poly[ind:]


                elif intr[0] == alles[-1]:
                    ind = intr[0]+acc
                    poly.insert(ind, intr[1])

                    if length_polyline(poly[ind:]) < 15 and len(poly[ind+1:]) < 3:
                        poly = poly[:ind+1]

                else:
                    ind = intr[0]+acc
                  
                    poly.insert(ind, intr[1])
                    acc += 1
                    prev_ind = ind

            dataToUpdate[link] = poly

    return dataToUpdate

def updateForIntersec(featClass, link_data, updated, hasObjectID = False):
    """
    INPUT:
    featClass: (string) name/path to feature class to be updated
    link_data: (list) output value of getIntersecIndex function
    updated: (dict) dictionary containg raw geometry values for polylines
            and is already updated for intersection values (used for "infc")
    hasObjectID: (boolean) checker if featClass is shapefile or feature Class in geodatabase

    FUNCTION:
    Takes updated geometry (lines split at intersection), deletes non-split lines and inserts new
    geometry into featClass on the same location as deleted geometry but with split lines at intersections

    Differs from update vertices with intersection funciton as it splits lines at intersections and makes more
    small features. It can create new polylines only containing 2 points

    OUTPUT:
    Function returns atributes of deleted features and id link from deleted to new features.

    """

    #######################################################

    ids = [j[1] for j in link_data]
    id_set = sorted(set(ids), reverse = True)


    fields = ["UL_MID", "KATEGORIJA", "NIVO","Kategorija_t","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]

    attrs = {}

    for j in id_set:
        statement = "OBJECTID = "+str(j)
        with arcpy.da.SearchCursor(featClass, fields, statement) as sCur:
            for row in sCur:
                attrs[j] = row[:]

    if hasObjectID:
        for i in id_set:
            statement = "OBJECTID = "+str(i)
            with arcpy.da.UpdateCursor (featClass, ("SHAPE@"), statement) as update:
                for row in update:
                    update.deleteRow()
        
    else:
        for i in id_set:
            statement = "FID = "+str(i)
            with arcpy.da.UpdateCursor (featClass, ("SHAPE@"), statement) as update:
                for row in update:
                    update.deleteRow()

    new_ids = []

    with arcpy.da.InsertCursor(featClass, ("SHAPE@")) as i_cur:
        polys, key_link = dict2Polylines2(updated)
        for poly in range(len(polys)):
            if polys[poly].length > 0.0001:
                new_ids.append([])
                new_ids[poly].append(i_cur.insertRow([polys[poly]]))
                new_ids[poly].append(key_link[poly])

    return attrs, new_ids

def splitAtEnds(existing, inBeetw, hasObjectID = False):
    """
    Splits existing selcted polylines at vertices given in inBeetw parameter

    Function in used when spliting existing features to determine best possible
    overlaping features. New splited polylines have same attributes as original feature
    """
    ids = [j[0] for j in inBeetw]
    id_set = sorted(set(ids), reverse = True) 
    exAttrs = {}
    fields = ["UL_MID", "KATEGORIJA", "NIVO","Kategorija_t","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]

    for i in ids:
        
        if hasObjectID:
            nameField = "OBJECTID"
            statement = '"' + nameField + '" = ' + str(i) 
            update = arcpy.da.SearchCursor (existing, fields, statement)
            
            for row in update:
                exAttrs[i] = row[:]
        else:
            nameField = "FID"
            statement = '"' + nameField + '" = ' + str(i)  
            update = arcpy.da.SearchCursor (existing, fields, statement)
            
            for row in update:
                exAttrs[i] = row[:]
        del update

    for i in ids:
        if hasObjectID:
            nameField = "OBJECTID"
            statement = '"' + nameField + '" = ' + str(i) 
            update = arcpy.da.UpdateCursor (existing, ("OID@"), statement)
            
            for row in update:
                update.deleteRow()
        else:
            nameField = "FID"
            statement = '"' + nameField + '" = ' + str(i)  
            update = arcpy.da.UpdateCursor (existing, ("OID@"), statement)
            
            for row in update:
                update.deleteRow()
        del update

    new_ids = []

    with arcpy.da.InsertCursor(existing, ("SHAPE@")) as iCur:
        polys, key_link = dict2Polylines2(inBeetw)
        for poly in range(len(polys)):
            if polys[poly].length > 0.0001:
                new_ids.append([])
                new_ids[poly].append(iCur.insertRow([polys[poly]]))
                new_ids[poly].append(key_link[poly])

    return exAttrs, new_ids

def check_connection_in(shp_data):
    '''
    edge_data = izhodna spremenljivka funkcije read_in_geom
    
    funkcija preveri povezave za vhodni feature class. Ohrani le vnose z
    visecimi povezavami oz. z nepovezanimi feature-ami
    
    Struktura podatkov: 
    dict_with_Data = { tmp_ip1: [[x 1st, y 1st], [x last, y last],stat], tmp_ip2: [-||-], ... }

    stat = 0 - ni povezave: default
    1- povezava na prvi tocki
    2- povezava na zadnji tocki
    3- povezava na obeh (prvi in zadnji- pomeni, da ustrezno umescen feature(linija))
    

    '''
    
    keys = list(shp_data.keys())
    
    edge_data = {}
    
    for j in keys:
        #pripni prvo in zadnjo tocko dela ter 0
        edge_data[j] = [shp_data[j][0], shp_data[j][-1], 0]

    for edge in edge_data:
        for i in keys:
            if i == edge:
                continue

            elif edge_data[edge][0] in shp_data[i]:
                if edge_data[edge][2] == 2 :
                    edge_data[edge][2] = 3
                elif edge_data [edge][1] in shp_data[i]:
                    edge_data[edge][2] = 3
                else:
                    edge_data[edge][2] = 1 

            elif edge_data[edge][1] in shp_data[i]:
                if edge_data [edge][2] == 1:
                    edge_data [edge][2] = 3
                elif edge_data [edge][0] in shp_data[i]:
                    edge_data[edge][2] = 3
                else:
                    edge_data[edge][2] = 2 

    #izbrisi, ce je linija ustrezno umescena
    for j in keys:
        if edge_data[j][2] == 3:
            del edge_data[j]
    return edge_data

def delOverlay(existing, overlapID, hasObjectID = False):

    overlapKeys = sorted(list(overlapID.keys()), reverse = True)
    fields = ["UL_MID", "KATEGORIJA", "NIVO","Kategorija_t","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]
    attrs = []

    #vzami atribute z najvecjim stevilom bliznjih tock
    #in shrani njihov id
    maxPnts = [0,0]
    for i in overlapKeys:
        if overlapID[i][0] > maxPnts[0]:
            maxPnts = [overlapID[i][0], i]


    #najdi izbrane atribute za brisane feature-je, ki imajo
    #najvec bliznjih tock v listu
    if hasObjectID:
        nameField = "OBJECTID"
        statement = '"' + nameField + '" = ' + str(maxPnts[1])
        with arcpy.da.SearchCursor(existing, fields, statement) as sCur:
            for row in sCur:
                for field in row:
                    attrs.append(field)

    else:
        nameField = "FID"
        statement = '"' + nameField + '" = ' + str(maxPnts[1])
        with arcpy.da.SearchCursor(existing, fields, statement) as sCur:
            for row in sCur:
                for field in row:
                    attrs.append(field)

    #izbrisi featurje
    for i in overlapKeys:
        
        if hasObjectID:
            nameField = "OBJECTID"
            statement = '"' + nameField + '" = ' + str(i) 
            update = arcpy.da.UpdateCursor (existing, ("SHAPE@"), statement)
            
            for row in update:
                update.deleteRow()
        else:

            nameField = "FID"
            statement = '"' + nameField + '" = ' + str(i)
             
            update = arcpy.da.UpdateCursor (existing, ("SHAPE@"), statement)
            for row in update:
                update.deleteRow()
        del update

    return attrs


def updateDataset(existing, pntsForUpdate, hasObjectID = False):
    ids = list(pntsForUpdate.keys())
    for i in ids:

        if hasObjectID:
            nameField = "OBJECTID"
            statement = '"' + nameField + '" = ' + str(i) 
            update = arcpy.da.UpdateCursor (existing, ("SHAPE@"), statement)
            
            for row in update:
                row[0] = pntsForUpdate[i]
                update.updateRow(row)
        else:
            nameField = "FID"
            statement = '"' + nameField + '" = ' + str(i)  
            update = arcpy.da.UpdateCursor (existing, ("SHAPE@"), statement)
            
            for row in update:
                row[0] = pntsForUpdate[i]
                update.updateRow(row)

def insert_new(dataset, read_infc_geom, overlap):

    maxPnts = [0,0]
    for i in overlap:
        if overlap[i][0] > maxPnts[0]:
            maxPnts = [overlap[i][0], i[0]]

    #izbrisi stare featurje
    for oid in overlap:
        statement = '"OBJECTID" = ' + str(oid[0])
        with arcpy.da.UpdateCursor(dataset, ("*"), statement) as ucur:
            for row in ucur:
                ucur.deleteRow()

    new_ids = []
    with arcpy.da.InsertCursor(dataset, ("SHAPE@")) as i_cur:
        polys, key_link = dict2Polylines1(read_infc_geom)
        for poly in range(len(polys)):
            if polys[poly].length > 0.0001:
                id_nov = i_cur.insertRow([polys[poly]])
                new_ids.append(int(id_nov))
                arcpy.AddMessage(id_nov)
                new_ids.append(maxPnts[1])

    return new_ids

def updateAttrs(dataset, attrs, new_attrs, id_link, Overlap):

    arcpy.SelectLayerByAttribute_management(dataset, "CLEAR_SELECTION")
    fields = ["ODSEK", "KATEGORIJA", "DAT_EL", "DAT_POSOD", "Kategorija_t", "NIVO","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]
    
    nameField = "OBJECTID"
    statement = '"' + nameField + '" = ' + str(id_link[0])
    arcpy.AddMessage(statement)

    with arcpy.da.UpdateCursor(dataset, fields, statement) as uCur:
        for row in uCur:
            arcpy.AddMessage("updating inserted attributes at")

            if Overlap: 
                row [0:5] = new_attrs[0:5]
                row[5:12] = attrs[id_link[1]][0:7]
                uCur.updateRow(row)

            else:
                row [0:5] = new_attrs[0:5]
                row[5:12] = attrs[0:7]
                uCur.updateRow(row)

def prompt4paths():
    
    geodb_path = raw_input("Directory of geodatabase and feature class: ") 
    arcpy.AddMessage("Select input shapefile: ")
    root = tk.Tk()
    root.withdraw()
    infc_path = tkFileDialog.askopenfilename()

    arcpy.AddMessage("Select directory to save temporary/help files")
    tmp_dir = tkFileDialog.askdirectory()

    return infc_path, geodb_path, tmp_dir


def appendAttributes(existing,csvFilename, IDlink):
    """
    csvFilename: name of the txt/csv table file
    IDlink: (list) returned value of insert_to_existing function


    """

    fields, attVal = readAttributes(csvFilename)

    for i in IDlink:
        try:
            attr = attVal[i[0]]
            attr = [j if j != "" else 0 for j in attr]

            nameField = "OBJECTID"
            statement = '"' + nameField + '" = ' + str( int(i[1]) )  

            fields, attVal = readAttributes(csvFilename)
            with arcpy.da.UpdateCursor(existing, fields, statement) as uCur:
                for row in uCur:
                    row = attr
                    uCur.updateRow(row)

        except:
            continue

def readAttributes(filename) : 

    with open(filename) as f:
        allLines = f.read().splitlines() 

    fields =  allLines [0]
    values = allLines[1:]

    fields = fields.split(";")[1:]
    valuesParsed = {}

    for row in values:
        valSplit = row.split(";")
        fid = int(valSplit[0])
        valuesParsed[fid] = valSplit[1:]

    return (fields, valuesParsed)



def extendLines(dataset, max_length):
    '''
    INPUT:
    dataset = feature class, on which the features will be selected.
    infc = input feture class to be merged in dataset -
    used for selecting nearest.
    max_length = distance for maximum extension in METERS.
    
    FUNCTION:
    Selects closest features to "infc" from dataset and executes
    extend line to fix hanging connections that are close to input feature("infc").
    
    Dataset has to already be updated
    with features from "infc"!!! 
    
    OUTPUT:
    No output
    '''
    dist  = str(max_length)+" Meters"

    #arcpy.SelectLayerByLocation_management(dataset, 'WITHIN_A_DISTANCE', infc, dist , 'NEW_SELECTION', 'NOT_INVERT')
    
    arcpy.ExtendLine_edit(dataset, dist, 'FEATURE')

def trimLines(dataset, dangle_length):
    """
    INPUT:
    dataset = feature class, on which the features will be selected.
    infc = input feture class to be merged in dataset -
           used for selecting nearest.
    dangle_length = distance for maximum length for feature to qualify as dangle in METERS.
    
    FUNCTION:
    Select features close to "infc" from dataset and executes trim lines
    arcgis function that deletes unnecesery hagning connections with
    specified length

    OUTPUT:
    No output

    """
    dist = str(dangle_length)+" Meters"

    #arcpy.SelectLayerByLocation_management(dataset, 'WITHIN_A_DISTANCE', infc, dist , 'NEW_SELECTION', 'NOT_INVERT')

    arcpy.TrimLine_edit(dataset, dist)


def isInterNotEnd(inter_coords, endpoints):

    checker = []
    for i in inter_coords:

        for j in endpoints:
            if euclideanDist(i, endpoints[j][0]) < 2:
                checker.append(False)

            else:
                checker.append(True)
    
    if all(check is True for check in checker):
        return True
    else:
        return False

def createSpatialIndex(existing_db, y_n):
    #y_n = str(input("Do you want to build spatial index on existing database? (y/n) "))

    if y_n == "y":
        arcpy.AddMessage("Building spatial index...")
        arcpy.AddSpatialIndex_management(existing_db)       
    elif y_n == "n":
        arcpy.AddMessage("You selected not to build spatial index")
    else:
        arcpy.AddMessage("No y/n input... skipping this process")