import arcpy
import numpy as np

def lenPtn2Line(p1, p2, p3):
    """
    p1, p2: points that make line
    p3 : point to calculate distance from line p1p2
    
    returns: lenght from point to line defined by 2 points
    """
    p1 = np.asarray(p1)
    p2 = np.asarray(p2)
    p3 = np.asarray(p3)

    return np.linalg.norm(np.cross(p2-p1, p1-p3))/np.linalg.norm(p2-p1)

def point_on_line(l1,l2, xy):
    
    l1 = [float(l1[0]), float(l1[1])]
    l2 = [float(l2[0]), float(l2[1])]
    
    xy = [float(xy[0]), float(xy[1])]
    
    k = ((l2[1]-l1[1]) * (xy[0]-l1[0]) - (l2[0]-l1[0]) * (xy[1]-l1[1])) / ((l2[1]-l1[1])**2 + (l1[0]-l2[0])**2)
    xn_line = xy[0] - k * (l2[1]-l1[1])
    yn_line = xy[1] + k * (l2[0]-l1[0])
    
    return xn_line, yn_line

def euclideanDist (p1, p2):
    """
    input:
    p1 = x,y coordinates saved in list [x1, y1]
    p2 = x,y coordinates saved in list [x2, y2]

    returns distance between points
    """
    return np.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2) 

def dict2Polylines1(dictClass):
    """
    INPUT:
    dictClass = (dict) python dictionary object containing raw geomery of polylines
                structure is -> {ID_1 : [[x1, y1], [x2, y2], [x3, y3],...], ID_2: [[x1, y1], [x2,y2],...],...}
    FUNCTION:
    Takes dictionary and returns list of Arcpy polyline objects. Number of polylines is identical to number of keys
    in dictionary and uses raw point values from dictionary. 

    This function is neccessery for updateForIntersec function! The only way for writing new geometry into datasets
    is with creation of Arcpy geometry objects.

    OUTPUT:
    polyline: (list) a list of polyline objects
    """
    polylines = []

    ptns = []
    key = 0

    for ptn in dictClass:
        ptns.append(arcpy.Point(ptn[0], ptn[1]))

    if len(ptns)>1:
        arr = arcpy.Array(ptns)
        poly = arcpy.Polyline(arr)
        if poly.length > 0.0001:
            polylines.append(poly)


    return polylines

def dict2Polylines2(dictClass):
    """
    INPUT:
    dictClass = (dict) python dictionary object containing raw geomery of polylines
                structure is -> {ID_1 : [[x1, y1], [x2, y2], [x3, y3],...], ID_2: [[x1, y1], [x2,y2],...],...}
    FUNCTION:
    Takes dictionary and returns list of Arcpy polyline objects. Number of polylines is identical to number of keys
    in dictionary and uses raw point values from dictionary. 

    This function is neccessery for updateForIntersec function! The only way for writing new geometry into datasets
    is with creation of Arcpy geometry objects.

    OUTPUT:
    polyline: (list) a list of polyline objects
    """
    polylines = []
    keys = list(dictClass.keys())
    keys_out = []

    for key in keys:
        ptns = []

        for ptn in dictClass[key]:
            ptns.append(arcpy.Point(ptn[0], ptn[1]))

        if len(ptns)>1:
            arr = arcpy.Array(ptns)
            poly = arcpy.Polyline(arr)
            if poly.length > 0.0001:
                polylines.append(poly)
                keys_out.append(key[0])

    return polylines, keys_out

def createTmpClass(path, name, existing):

    """ creates tmp class with only geometry.
    This class is only used for selecting by location and then read in python dict"""

    arcpy.CreateFeatureclass_management(path, name, "POLYLINE")

    with arcpy.da.InsertCursor("temp", ("SHAPE@")) as i_cur:
        with arcpy.da.SearchCursor(existing, ("SHAPE@")) as s_cur:
            for srow in s_cur:
                i_cur.insertRow(srow)

def read_in_geom(infc):
    """
    infc - input feature class. Prebrana geometrija za posamezen feature iz *.shpja

    returns: geometrija polylinije kot list listov [[xi, yi], [...], [...],...]
    """
    shp_data = []
    with arcpy.da.SearchCursor(infc, ("OID@", "SHAPE@")) as rows:
        for row in rows:
            for i in row[1]:
                for j in i:
                    point = [j.X, j.Y]
                    shp_data.append(point)
    return shp_data

def select_loc (existing, infc, dist):
    
    dist  = str(dist)+" Meters"
    arcpy.SelectLayerByLocation_management(existing, 'WITHIN_A_DISTANCE', infc, dist , 'NEW_SELECTION', 'NOT_INVERT')
    
    with arcpy.da.SearchCursor(existing, ("OID@","SHAPE@")) as selected:

        selected_data = {}
        for row in selected:
            
            selected_data[(row[0],0)] = []

            for part in row[1]:

                for pnt in part:
                    if pnt:
                        selected_data[(row[0],0)].append([pnt.X, pnt.Y])
            
        return selected_data

def updateAll(dataset, dict_data):


    oIDS = set([ i[0] for i in dict_data ])

    fields = ["SHAPE@", "ODSEK", "KATEGORIJA", "DAT_EL", "DAT_POSOD", "Kategorija_t", "NIVO","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]
    attrs = {}

    #pridobi atribute
    for oid in oIDS:
        statement = "OBJECTID = " + str(oid)
        with arcpy.da.SearchCursor(dataset, fields, statement) as scur:
            for row in scur:
                attrs[oid] = row[1:]
        del scur

    #izbrisi stare featurje
    for oid in oIDS:
        statement = "OBJECTID = " + str(oid)
        with arcpy.da.UpdateCursor(dataset, ("*"), statement) as ucur:
            for row in ucur:
                ucur.deleteRow()
        del ucur

    arcpy.SelectLayerByAttribute_management(dataset, "CLEAR_SELECTION")

    #vstavi nove featurje
    with arcpy.da.InsertCursor(dataset, fields) as i_cur:

        polys, key_link = dict2Polylines2(dict_data)

        for poly in range(len(polys)):

            if polys[poly].length > 0.0001:
                #geometrija (@shape) zdruzena v list z vsemi navedenimi atributi
                insert_list = [polys[poly]] + list(attrs[key_link[poly]])
                i_cur.insertRow(insert_list)

        del i_cur


def insert_update_attr (dataset, infeat , overlayFeat, old_a, new_a, isOverlay):

    fields = ["SHAPE@", "ODSEK", "KATEGORIJA", "DAT_EL", "DAT_POSOD", "Kategorija_t", "NIVO","MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]

    #prilagodi izpis za kategorija_t : str(atr1 + " - " + atr2)
    new_a[-1] = renameCategory(new_a[1], new_a[-1])

    maxPnts = [0,0]
    for i in overlayFeat:
        if overlayFeat[i][0] > maxPnts[0]:
            maxPnts = [overlayFeat[i][0], i[0]]

    #izbrisi stare featurje
    for oid in overlayFeat:
        statement = "OBJECTID = " + str(oid[0])
        with arcpy.da.UpdateCursor(dataset, ("*"), statement) as ucur:
            for row in ucur:
                ucur.deleteRow()
        del ucur

    with arcpy.da.InsertCursor(dataset, fields) as i_cur:
        polys = dict2Polylines1(infeat)
        for poly in polys:
            if isOverlay:
                insert_list = [poly] + list(new_a[0:5]) + list(old_a[maxPnts[1]][0:7])
                a = i_cur.insertRow(insert_list)
                arcpy.AddMessage("new_feautre id: {}, old: {}".format(a, maxPnts[1]))
            else:
                insert_list = [poly] + list(new_a[0:5]) + list(old_a[0:7])
                a = i_cur.insertRow(insert_list)
                arcpy.AddMessage("new_feautre id: {}, old: {}".format(a, maxPnts[1]))

def renameCategory(atr1, atr2):
    atr2 = int(atr2)
    rename_dict = {
        1 : "Most",
        2 : "Nadvoz",
        3 : "Podvoz",
        4 : "Zeleznica",
        5 : "Viadukt",
        6 : "Predor"
    }
    if atr2 in rename_dict:
        return str(atr1) + " - " + str(rename_dict[atr2])
    else:
        return str(atr1)
