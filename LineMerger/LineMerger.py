import arcpy
from datetime import datetime, date
import time
from logger import logger
from generalFunctions import *
from overlapFunctions import *
from endpointFunctions import *
from intersecFunctions import *
from extraNotUsedFunc import *
from LineMergerProcess import *

class LineMerger:
    def __init__(self, in_shp, existing_db, temp_dir):
        self.temp_dir = temp_dir
        self.shp = in_shp
        self.db = existing_db

        arcpy.env.addOutputsToMap = 0
        arcpy.env.overwriteOutput = True

        self.cClassPath = temp_dir+"\\controlClass.shp"
        arcpy.CreateFeatureclass_management(temp_dir ,"controlClass.shp", "POLYGON")
        arcpy.AddField_management (self.cClassPath, "DAT_POSOD", "DATE")
        arcpy.AddField_management (self.cClassPath, "PREK", "SHORT")

        arcpy.MakeFeatureLayer_management(self.db, "existing")

    def run(self):

        #generiranje sekvence vseh linij v shp-ju
        shape_len = 0
        readShpFids = []
        with arcpy.da.SearchCursor(self.shp, "*") as sCur:
            for row in sCur:
                readShpFids.append(row[0]) 
                shape_len += 1

        #counter za postopno merganje in sproscanje rama
        atOnce = 10
        start = 0
        counter = [start, start+atOnce]

        start_whole = time.time()

        # prev_memory = int(psutil.virtual_memory()[3])
        extents = None
        while counter[1] <= shape_len:
            start = time.time()
            extents = processEachFeature(self.temp_dir, self.shp, "existing", readShpFids, self.cClassPath, counter)
            counter[0] += atOnce
            counter[1] += atOnce
            stop = time.time()

            # arcpy.AddMessage("Memory:")
            # arcpy.AddMessage(int(psutil.virtual_memory()[3])- prev_memory)
            # prev_memory = int(psutil.virtual_memory()[3])

            arcpy.AddMessage("========================================================================")
            arcpy.AddMessage("Posodobil sem {} featurjev".format(atOnce))
            arcpy.AddMessage("Proces za posodobitev {} featurjev je trajal {}s".format(atOnce, stop-start))
            arcpy.AddMessage("========================================================================")

        if counter[1]> shape_len:
            start = time.time()
            residual = shape_len%atOnce
            counter = [counter[0], counter[0]+residual]
            extents = processEachFeature(self.temp_dir, self.shp, "existing", readShpFids, self.cClassPath, counter)
            stop = time.time()

            # arcpy.AddMessage("Memory:")
            # arcpy.AddMessage(int(psutil.virtual_memory()[3])- prev_memory)
            # prev_memory = int(psutil.virtual_memory()[3])

            arcpy.AddMessage("=======================================================================")
            arcpy.AddMessage("Posodobil sem {} featurjev".format(residual))
            arcpy.AddMessage("Proces za posodobitev {} featurjev je trajal {}s".format(residual, stop-start))
            arcpy.AddMessage("=======================================================================")

        stop_whole = time.time()
        arcpy.AddMessage("Proces merganja je trajal: {}m {}s".format( round((stop_whole-start_whole)/60,0), round((stop_whole-start_whole)%60,2) ))
        return extents


    def trim_extend(self):
        arcpy.AddMessage("Izvajam trim/extend line... Ta postopek lahko traja par minut odvisno od racunalnika in velikosti baze")

        lines_start = time.time()
        select_loc("existing", self.shp, 15)
        extendLines("existing", 3)
        trimLines("existing", 5)
        lines_stop = time.time()
        arcpy.AddMessage("Proces trim/extend lines je trajal {}m {}s.".format(round((lines_stop-lines_start)/60,0), round((lines_stop-lines_start)%60, 2) ))
        

if __name__ == "__main__":
    #TESTS
    shp_file_path = r"D:\blaz\karto_podatki\GJI_test1.shp"
    ceste_path = r"D:\blaz\karto_podatki\podatki.gdb\GJI_ceste_zeleznice_1"
    ceste_smooth_path = r'D:\blaz\karto_podatki\podatki.gdb\GJI_ceste_zeleznice'
    temp_path = r"D:\blaz\karto_podatki\tmp"

    lm = LineMerger(shp_file_path, ceste_path, temp_path)
    lm.run()
    lm.trim_extend()