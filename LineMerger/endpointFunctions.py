import numpy as np
from generalFunctions import euclideanDist, point_on_line, lenPtn2Line
from overlapFunctions import lenPtn2Line

def get_endptns(shp_data):
    """ Vrne prvo in zadnjo tocko feature-ja iz uvoznega shp-ja """
    return [shp_data[0], shp_data[-1]]

def closestPntMatch(edge_points, exdata_points, toleranca):
    '''
    input: edge_points = output dict of edge_points funcition (only non connected features on one side)
            exdata_points = output dict select_loc dictionary (function uses arcGis functionality to import
            feature class and select closest feature from other class and returns them as vertices)

    output: dict, out = {tmp_id: [[points of inputFeatureClass], [distance, ID from original dataset, index number of vertex]]}
    
    Iterate through edge points and check for closest vertex in existing dataset.
    
    '''
    exdata_keys = list(exdata_points.keys())
    
    link_data = {}

    e_ptn = edge_points[:]

    count = 1
    for ptn in e_ptn:
        link_data[count] = []

        #struktura podatkov:
        #output = {id: [[xt, yt], [minimalna razdalja, uid iz baze, indeks tocke v feature]], ...}
        
        link_data[count].append(ptn)

        for id_ex in exdata_keys:
            min_dist = [9999999.0, 0, 0]

            for ver in exdata_points[id_ex]:
                dist = euclideanDist(ptn, ver)

                if dist < min_dist[0]:
                    min_dist = [dist, id_ex, exdata_points[id_ex].index(ver)] 

            if min_dist[0] < toleranca:
                link_data[count].append(min_dist)

        count += 1
        
    return link_data

def snapCloseGeom(matchedData, exdata_points):
    """
    Snap-anje najblizjih tock obstojece baze ("exdata", as existing data) in jih snapa
    na najblizji verteks v uvoznem shape-u.

    Funkcija ne snapa eksplicitno koncev obstojece baze
    """
    pntsForUpdate = {}
    del_count = {}
    upd_count = {}

    #vzpostavi dict z belezenjem counterja
    #za vsak id iz matched point {1: [[x,y], [minD, oid, ind], [midD, oid, ind] , .. ], 2: [...] }
    for m_ptn in matchedData:
        for ver in matchedData[m_ptn][1:]:
            del_count[ver[1]] = 0
            upd_count[ver[1]] = 0
    
    #za vsako "koncno tocko uvoznega shpja"
    for i in matchedData:

        #vzpostavi koordinate tocke
        newCoords = matchedData[i][0]

        #vse ID-je in ind za feature
        exIDs = [j[1] for j in matchedData[i][1:]]
        exSeqs = [j[2] for j in matchedData[i][1:]]
        for exi in range(len(exIDs)): #exi = int indeks
            
            #dejanske vrednosti oid-jev in indeksov
            exID = exIDs[exi]
            exSeq = exSeqs[exi]

            #ce linija vsebuje vec kot 1 tocko- mora biti
            if len(exdata_points[exID]) > 1:

                if upd_count[exID] > 0: #counter, ki kolikokrat je posodobljen
                    pntsForUpdate[exID][exSeq-del_count[exID]] = newCoords
                    upd_count[exID] +=1 

                else: #kopiraj podatke le, ce se ze niso posodobili v tem procesu
                    pntsForUpdate[exID] = exdata_points[exID][:]
                    pntsForUpdate[exID][exSeq-del_count[exID]] = newCoords
                    upd_count[exID] +=1
            else:
                continue

            #ce ni linija in je samo tocka
            if len(pntsForUpdate[exID]) == 1:
                del pntsForUpdate[exID]

            #preveri ce ostane le en kratek kos polylinije (dangle) in ga zbrisi
            elif len(pntsForUpdate[exID][exSeq+1:]) == 1:
                dangleLen = euclideanDist(pntsForUpdate[exID][exSeq+1], pntsForUpdate[exID][exSeq])

                if dangleLen < 5: # 10 metrov - mozno implementiranje novega parametra
                    del pntsForUpdate[exID][exSeq+1]
                    del_count[exID] += 1

            elif exSeq == 1:
                dangleLen = euclideanDist(pntsForUpdate[exID][0], pntsForUpdate[exID][1])

                if dangleLen < 5: # 10 metrov - mozno implementiranje novega parametra
                    del pntsForUpdate[exID][0]
                    del_count[exID] += 1


    exdata_points.update(pntsForUpdate)
    return exdata_points


def insertVerEnd(edges, exdata, tolerance):
    """
    vstavi vertekse na mesto pravokotne projekcije tocke
    na linijski segment, katere premik je manjsi od tolerance
    """
    out_data = exdata.copy()
    for end in edges:

        for part in exdata:
            acc = 1
            min_dist = []
            #za vsak segment polilinije
            for ver in range(len(exdata[part])-1):
                line = [ exdata[part][ver], exdata[part][ver+1] ]

                #izracun pravokotne razdalje na linijo
                length = lenPtn2Line(line[0], line[1], end)

                if length <= tolerance:
                    #ce je blizu se izracuna koordinate tocke na liniji
                    x, y = point_on_line(line[0], line[1], end)

                    dist_line = euclideanDist(line[0], line[1])
                    dist_1 = euclideanDist(line[0], [x,y])
                    dist_2 = euclideanDist(line[1], [x,y])

                    #preverjanje ce izracunana tocka lezi na segmentu
                    #in ce so obstojeci verteksi dalec

                    if dist_1 > 1 and dist_2 > 1:
                        if abs(dist_line-(dist_1+dist_2))<= 0.01:
                            out_data[part].insert(ver+acc, [x,y])

                            arcpy.AddMessage("distall: {}, dist1: {}, dist2: {}".format(dist_line, dist_1, dist_2))
                            arcpy.AddMessage("inserted in part: {} on ver: {}".format(part, ver+acc))
                            arcpy.AddMessage("\n")
                            acc += 1
                        
    return out_data

def snapEnds(infc, exdata_v, tolerance):
    """
    Takes endpoints of each feature and snaps them to closest
    verteks in infc if that vertex is closer than tolerance
    """
    for part in exdata_v:
        end1 = exdata_v[part][0]
        end2 = exdata_v[part][-1]

        end1_list = []
        end2_list = []

        #calc all dists and ptns and append them to saving vars

        for ptn in infc: 
            end1_list.append([euclideanDist(end1, ptn), ptn])
            end2_list.append([euclideanDist(end2, ptn), ptn])
        
        # sorts appended by dist to closest vector
        end1_len = sorted(end1_list, key = lambda x: x[0] )[0]
        end2_len = sorted(end2_list, key = lambda x: x[0] )[0] 


        #checking if the smallest is less than tolerance allows
        if end1_len[0] < tolerance:
            exdata_v[part][0] = end1_len[1]

        if end2_len[0] < tolerance:
            exdata_v[part][-1] = end2_len[1]

    return exdata_v
