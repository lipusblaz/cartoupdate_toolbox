import arcpy
import numpy as np
from generalFunctions import euclideanDist, lenPtn2Line, point_on_line
from numpy.linalg import lstsq
from numpy import vstack

def point_on_line(l1,l2, xy):
    """
    ob dani liniji, definiranimi s dvema tockama
    izracuna koordinate blizne tocke ("xy") na premici(liniji) med "l1" in "l2"
    """

    #pretvorba koordinat v float za korekten izuracun
    l1 = [float(l1[0]), float(l1[1])]
    l2 = [float(l2[0]), float(l2[1])]
    xy = [float(xy[0]), float(xy[1])]
    
    #Izracun - enacba
    k = ((l2[1]-l1[1]) * (xy[0]-l1[0]) - (l2[0]-l1[0]) * (xy[1]-l1[1])) / ((l2[1]-l1[1])**2 + (l1[0]-l2[0])**2)
    xn_line = xy[0] - k * (l2[1]-l1[1])
    yn_line = xy[1] + k * (l2[0]-l1[0])
    
    return xn_line, yn_line

def length_polyline(list_ver):
    """
    Takes polyline segment as list and outputs segment length

    funkcija deluje komulativno
    """
    lenP = 0
    for i in range(len(list_ver)-1):
        lenP += euclideanDist(list_ver[i], list_ver[i+1])
    return lenP


def line_eq (ptn1, ptn2):
    """
    Vzame 2 tocki (ptn1, ptn2) oblike [x1, y1] in [x2, y2] ter izracuna
    koeficiente premice in s tem poda enacbo premice
    
    m = naklon premice
    c = zacetna vrednost premice

    """

    points = [ptn1, ptn2]
    x_k, y_k = zip(*points)
    A = vstack([x_k, np.ones(len(x_k))]).T
    m, c  = lstsq(A, y_k)[0]
    return m, c

def densifyPoly(exdata_part):
    """
    Featerju doda vertekse, brez da bi s tem vplival na dejansko
    geometrijo. Dodaja jih le na ravne linije med obstojecimi verteksi.

    Input: (list) z geometrijo
    Returns: (list) feature z dodanimi verteksi 
    """
    part_c = exdata_part[:]
    
    #dodajanje verteksov za boljsi priblizek
    if len(part_c) < 10 and length_polyline(part_c) > 5:
        acc = 1

        for point in range(len(exdata_part)-1):
            p11 = exdata_part[point]
            p22 =exdata_part[point+1]
            #p11 p22 definirata linijo

            dist_eu = euclideanDist(p11, p22)
            m, c = line_eq(p11, p22)
            
            if dist_eu > 10.0:
                st_dodanih_ver = int(dist_eu/2)
                rang = abs((p22[0]-p11[0])/st_dodanih_ver)
                
                for dodatni in range(1, st_dodanih_ver):
                    if dodatni != st_dodanih_ver:
                        
                        if p11[0]< p22[0]:
                            x_add = p11[0]+(dodatni*rang)
                            y_add = m*x_add + c
                            part_c.insert(point+acc, [x_add, y_add])
                            acc += 1

                        elif p22[0]< p11[0]:
                            x_add = p11[0]-(dodatni*rang)
                            y_add = m*x_add + c
                            part_c.insert(point+acc, [x_add, y_add])
                            acc += 1
                    else:
                        continue        
            else:
                x_mid = (p11[0]+p22[0])/2
                y_mid = m*x_mid + c
    
                part_c.insert(point+acc, [x_mid, y_mid])
                acc += 1
                
    return part_c

def splitAtEdges(exdata, matchedPoints):
    """
    exdata = dict, {ID-poly:[ [x,y], [...], ... ], ...}
    matchedPoints =  {1: [[x,y], [minD, oid, ind], [midD, oid, ind], ... ], 2: [...] }
    
    Returns: (Dict) posodobljenih featurjev - razbitih na dele pri preseciscih

    """

    #init saving variable
    endpoints = {}

    for fid in matchedPoints:

        for oid in matchedPoints[fid][1:]: #za [vse minD, oid, indeks]

            if oid[1] not in endpoints:
                endpoints[oid[1]] = []
                endpoints[oid[1]].append(oid[2])
            else:
                endpoints[oid[1]].append(oid[2])

    for oid1 in endpoints:
        endpoints[oid1] = sorted(endpoints[oid1])

    inBeetw = {}

    for oid in endpoints.keys():

        #ali se celoten feature nahaja na obstojecem- daljsem
        #polyline-u 
        uID = 10

        if len(endpoints[oid]) == 2:
            
            inBeetw[(oid[0], uID)] = exdata[oid][:endpoints[oid][0]+1] 
            inBeetw[(oid[0], uID+1)] = exdata[oid][endpoints[oid][0]: endpoints[oid][1]+1] 
            inBeetw[(oid[0], uID+2)] = exdata[oid][endpoints[oid][1]:]

            #izbrisi, ce ni linija in je samo tocka
            if len(inBeetw[(oid[0], uID)])<=1: 
                del inBeetw[(oid[0], uID)]

            if len(inBeetw[(oid[0], uID+1)])<=1:
                del inBeetw[(oid[0], uID+1)]

            if len(inBeetw[(oid[0], uID+2)]) <= 1:
                del inBeetw[(oid[0], uID+2)]

            uID += 3
            
        #ali se celoten feature nahaja na zacetku polyline-a 
        elif len(endpoints[oid]) == 1:

            inBeetw[(oid[0], uID)] = exdata[oid][:endpoints[oid][0]+1] 
            inBeetw[(oid[0], uID+1)] = exdata[oid][endpoints[oid][0]:] 

            if len(inBeetw[(oid[0], uID)])<=1:
                del inBeetw[(oid[0], uID)]

            if len(inBeetw[(oid[0], uID+1)])<=1:
                del inBeetw[(oid[0], uID+1)]

            uID += 2

    for k in endpoints:
        del exdata[k]

    exdata.update(inBeetw)

    return exdata

def getOverlapFeatures(infc, exdata, dist, tolerance, perc):
    """
    Takes each vertex in exdata and compares distance to closest line segment if it is 
    smaller than "dist". Sums all vertices closer and checks percentage of close 
    vertices
    """

    #izracun mej infc-ja
    ptns = [arcpy.Point(i[0], i[1]) for i in infc]
    infc_poly = arcpy.Polyline(arcpy.Array(ptns))

    ext = infc_poly.extent
    xy_min = [ext.XMin-dist, ext.YMin-dist] #pristevek, odstevek za vrednost
    xy_max = [ext.XMax+dist, ext.YMax+dist]

    overlapFeat = {}
    splitInfcSeg = []


    length_infc = length_polyline(infc) 

    for j in range(len(infc)-1):
        splitInfcSeg.append([infc[j], infc[j+1]])
    
    for part in exdata:
        ptnCount = 0
        partdists = []
        part_c = densifyPoly(exdata[part])

        #izracun mej exdata- obstojece baze     
        ptnsEx = [arcpy.Point(i[0], i[1]) for i in exdata[part]]
        exdata_poly = arcpy.Polyline(arcpy.Array(ptnsEx))

        ex_ext = infc_poly.extent
        exdata_min = [ex_ext.XMin-dist, ex_ext.YMin-dist]
        exdata_max = [ex_ext.XMax+dist, ex_ext.YMax+dist]

        #za vsak verteks linije obstojece baze
        for ver in part_c:
            segDists = []

            #verteks je kandidat, ce je v mejah obmocja

            if xy_min[0]<ver[0]<xy_max[0] and xy_min[1]<ver[1]<xy_max[1]:

                for i in range(len(splitInfcSeg)):

                    seg = splitInfcSeg[i] 

                    length = lenPtn2Line(seg[0], seg[1], ver)
                    
                    xl, yl = point_on_line(seg[0], seg[1], ver)

                    dist_line = euclideanDist(seg[0], seg[1])
                    dist_1 = euclideanDist(seg[0], [xl,yl])
                    dist_2 = euclideanDist(seg[1], [xl,yl])

                    #ce lezi projecirana tocka na segmentu
                    if abs(dist_line - (dist_1 + dist_2)) < 0.1:
                        segDists.append(length)


            if len(segDists)> 0:
                partdists.append(min(segDists))
            else:
                partdists.append(999.99)

        for i in partdists:
            if i < tolerance:
                ptnCount += 1

        partPerc = (float(ptnCount)/len(partdists)*100)
        
        # arcpy.AddMessage("part: "+str(part))
        # arcpy.AddMessage(partdists)
        # arcpy.AddMessage(len(partdists))
        # arcpy.AddMessage(str(partPerc)+" %")
        # arcpy.AddMessage(length_polyline(exdata[part]))

        #meja za procente, ki preverjajo prekrivanje se spremeni, ce je part snapan samo na eni strani
        #ali na obeh - ce je na obeh je moznost, da je to dejanska linija, ki se prekriva, ceprav je geometrija
        #manj podobna:

        if len(partdists) > 1:

            if abs(length_polyline(exdata[part]) - length_infc) < 10:
                perc = perc-8
                if partPerc >= perc: 
                    overlapFeat[part] = [ptnCount, partPerc,length_polyline(exdata[part])]
                    #arcpy.AddMessage("Overlap on {} for {} % with threshhold {}".format(part, partPerc, perc))
                perc = perc+8

            elif partdists[0] == 0.0 and partdists[-1] == 0.0 :
                perc = perc-5
                if partPerc >= perc: 
                    overlapFeat[part] = [ptnCount, partPerc,length_polyline(exdata[part])]
                    #arcpy.AddMessage("Overlap on {} for {} % with threshhold {}".format(part, partPerc, perc))
                perc = perc+5

            elif partdists[0] == 0.0 or partdists[-1] == 0.0:
                perc = perc+5
                if partPerc >= perc: 
                    overlapFeat[part] = [ptnCount, partPerc,length_polyline(exdata[part])]
                    #arcpy.AddMessage("Overlap on {} for {} % with threshhold {}".format(part, partPerc, perc))
                perc = perc-5

            else:
                if partPerc >= perc: 
                    overlapFeat[part] = [ptnCount, partPerc,length_polyline(exdata[part])]
                    #arcpy.AddMessage("Overlap on {} for {} % with threshhold {}".format(part, partPerc, perc))

    return overlapFeat

def delOverlap (dataset, exdataVer, link_data):

    attrs = {}
    fields = ["OBJECTID", "NIVO", "MOST","PREDOR","M_5000","IZPIS","Podvoz", "IZPIS_da_ne"]

    maxPnts = [0,0]
    for i in link_data:
        if link_data[i][0] > maxPnts[0]:
            maxPnts = [link_data[i][0], i]

    for i in link_data:
        del exdataVer[i]

    statement = "OBJECTID = " + str(maxPnts[1][0])
    with arcpy.da.SearchCursor(dataset, fields, statement) as scur:
        for row in scur:
            attrs[maxPnts[1][0]] = row[1:]

        del scur

    return exdataVer, attrs

def splitPartialOverlap(exdata, infc, tolerance):

    new_parts = {}
    
    #generiranje segmentov
    splitInfcSeg = []    
    for j in range(len(infc)-1):
        splitInfcSeg.append([infc[j], infc[j+1]])

    uid = 40
    del_id = []
    #iskanje overlapa
    for part in exdata:
        ptnCount = 0
        dist_bool = []
        part_c = densifyPoly(exdata[part])
        if len(exdata[part]) == 2:
            break
        for ver in part_c:
            seg_dists = []

            for i in range(len(splitInfcSeg)):
                #izracun med vsakim segmentom infc-ja in verteksom exdata- parta
                #izracun tocke na partu in izracun dolzine do linije
                seg = splitInfcSeg[i] 

                length = lenPtn2Line(seg[0], seg[1], ver)
                
                xl, yl = point_on_line(seg[0], seg[1], ver)

                dist_line = euclideanDist(seg[0], seg[1])
                dist_1 = euclideanDist(seg[0], [xl,yl])
                dist_2 = euclideanDist(seg[1], [xl,yl])

                #ce lezi projecirana tocka na segmentu
                if abs(dist_line - (dist_1 + dist_2)) < 0.1:
                    seg_dists.append(length)

            #vsak verteks se shrani v list kot boolean value
            #1 ce je verteks blizje od tolerance in 0 ce je dlje

            if len(seg_dists) == 0:
                dist_bool.append(0)
            else: #najmanjsa razdalja predstavlja dolzino do linije
                if min(seg_dists)< tolerance:
                    dist_bool.append(1)
                else:
                    dist_bool.append(0)


        if len(dist_bool) > 0:
            seqs = []
            c = dist_bool[0]

            for i in range(1, len(dist_bool)-1):
                if dist_bool[i] == c:
                    continue

                else:
                    if dist_bool[i+1] == c:
                        continue

                    else:
                        raz = i - c
                        c = dist_bool[i]

                        if len(dist_bool) > 16:
                            _25_perc = int(round(len(dist_bool)/4))
                        else:
                            _25_perc = 3

                        if raz >= _25_perc:
                            seqs.append(i)

            # arcpy.AddMessage(part, len(part_c))
            # arcpy.AddMessage(dist_bool)
            # arcpy.AddMessage(seqs)

            if len(seqs) > 0:
                acc = 0
                prev = 0

                if len(seqs) == 1:
                    if length_polyline(part_c[:seqs[0]+1])> 11 and length_polyline(part_c[seqs[0]-1:]) > 11:
                        new_parts[(part[0], uid+1)] = part_c[:seqs[0]]
                        new_parts[(part[0], uid+2)] = part_c[seqs[0]-1:]
                else:

                    for j in range(len(seqs)):
                        if length_polyline(part_c[prev:seqs[j]+1]) > 11:
                            new_parts[(part[0], uid+acc)] = part_c[prev:seqs[j]]
                            acc += 1
                            prev = seqs[j]
                        else: 
                            continue

                    new_parts[(part[0], uid+acc)] = part_c[prev-1:]

                del_id.append(part)

    for ID in del_id:
        del exdata[ID]

    exdata.update(new_parts)

    return exdata